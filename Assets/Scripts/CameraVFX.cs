﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraVFX : MonoBehaviour
{
    private int blurRadiusId;
    private int blurAlphaId;
    private int fadeOutAlphaId;
    
    private MeshFilter meshFilter;
    private Material blurMaterial;
    private Material fadeOutMaterial;
    private AudioSource audioSource;
    private AudioClip injectionAudio;


    private void Awake()
    {
        blurRadiusId = Shader.PropertyToID("_CamVFX_BlurRadius");
        blurAlphaId = Shader.PropertyToID("_CamVFX_Alpha");
        fadeOutAlphaId = Shader.PropertyToID("_FadeOut_Alpha");
        
        meshFilter = GetComponent<MeshFilter>();
        ReverseNormals();

        var materials = GetComponent<Renderer>().materials;
        audioSource = GetComponent<AudioSource>();
        injectionAudio = GameManager.instance.audioGlobal.player_injection;
        blurMaterial = materials[0];
        fadeOutMaterial = materials[1];
    }

    public IEnumerator WakeUp()
    {
        gameObject.SetActive(true);
        
        blurMaterial.SetFloat(blurAlphaId, .7f);
        yield return SleepingVFX();
        
        gameObject.SetActive(false);

    }
    public IEnumerator PassOut()
    {
        gameObject.SetActive(true);
        
        audioSource.PlayOneShot(injectionAudio);
        yield return SleepingVFX(3, reverse:true);
        
        blurMaterial.SetFloat(blurAlphaId, 0);
    }

    private IEnumerator SleepingVFX(float duration = 7f, bool reverse = false)
    {        
        float blurEnd = 0; 
        float fadeOutAlphaEnd = 0f;
        float blurAlphaEnd = 0f;
        float blurAlphaWaitTime = 0.8f;
        
        float blurStart = .1f;
        float fadeOutAlphaStart = 1f;
        float blurAlphaStart = .5f;

        if (reverse)
        {
            var _blurEnd = blurEnd;
            blurEnd = blurStart;
            blurStart = _blurEnd;

            var _fadeOutAlphaEnd = fadeOutAlphaEnd;
            fadeOutAlphaEnd = fadeOutAlphaStart;
            fadeOutAlphaStart = _fadeOutAlphaEnd;
            
            var _blurAlphaEnd = blurAlphaEnd;
            blurAlphaEnd = blurAlphaStart;
            blurAlphaStart = _blurAlphaEnd;

            blurAlphaWaitTime = 1 - blurAlphaWaitTime;
        }

        float t = 0;
        float blurValue;
        float fadeOutAlphaValue;
        float blurAlphaValue;
        while (t <= 1)
        {
            blurValue = Mathf.Lerp(blurStart, blurEnd, t);
            fadeOutAlphaValue = Mathf.SmoothStep(fadeOutAlphaStart, fadeOutAlphaEnd, t);
            blurMaterial.SetFloat(blurRadiusId, blurValue);
            fadeOutMaterial.SetFloat(fadeOutAlphaId, fadeOutAlphaValue);

            t += Time.deltaTime/duration;
            if (reverse)
            {
                if (t < blurAlphaWaitTime)
                {
                    blurAlphaValue = Mathf.Lerp(blurAlphaStart, blurAlphaEnd, t/blurAlphaWaitTime);
                    blurMaterial.SetFloat(blurAlphaId, blurAlphaValue);
                }
                else
                {
                    blurMaterial.SetFloat(blurAlphaId, blurAlphaEnd);
                }
            }
            else
            {
                if (t > blurAlphaWaitTime)
                {
                    blurAlphaValue = Mathf.Lerp(blurAlphaStart, blurAlphaEnd, (t-blurAlphaWaitTime)/(1-blurAlphaWaitTime));
                    blurMaterial.SetFloat(blurAlphaId, blurAlphaValue);
                }

            }

            yield return null;
        }
    }

    private void ReverseNormals()
    {
        var mesh = meshFilter.mesh;
        Vector3[] normals = mesh.normals;
        for (int i = 0; i < normals.Length; i++)
            normals[i] = -normals[i];
        mesh.normals = normals;

        for (int m = 0; m < mesh.subMeshCount; m++)
        {
            int[] triangles = mesh.GetTriangles(m);
            for (int i = 0; i < triangles.Length; i += 3)
            {
                int temp = triangles[i + 0];
                triangles[i + 0] = triangles[i + 1];
                triangles[i + 1] = temp;
            }

            mesh.SetTriangles(triangles, m);
        }
    }
    
}