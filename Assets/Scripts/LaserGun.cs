﻿using System;
using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using NewtonVR.Example;
using UnityEngine;
using Random = UnityEngine.Random;

public class LaserGun : NVRInteractable
{
    [Header("Laser Gun Specific")]
    
    public SheepShooterManager sheepShooterManager;
    
    public Vector3 positionOffset = new Vector3(0, 0, -0.1f);
    public Vector3 rotationOffset = new Vector3(-33, 0, -90);

    public GameObject firePoint;
    public ParticleSystem particleSystem;

    public Color laserColor;
    public float laserWidth = 0.02f;
    public float shotDisplayTime = 0.2f;
    public float cooldownTime = 0.5f;

    private AudioSource audioSource;
    private LineRenderer laser;
    private float timeSinceLastShot = 0f;

    private AudioClip laserGunShotSound;

    public Action OnShotInHead;
    public Action OnShotAI;

    private ParticleSystem.EmitParams emitParams;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = GameManager.instance.audioGlobal.sheep_laserGunShot;
        laser = GetComponent<LineRenderer>();

        if (laser == null)
        {
            laser = firePoint.AddComponent<LineRenderer>();
        }

        if (laser.sharedMaterial == null)
        {
            laser.material = new Material(Shader.Find("Unlit/Color"));
            laser.material.SetColor("_Color", laserColor);
            NVRHelpers.LineRendererSetColor(laser, laserColor, laserColor);
            NVRHelpers.LineRendererSetWidth(laser, laserWidth, laserWidth);
        }

        laser.useWorldSpace = true;


        particleSystem.transform.parent = null;

    }


    public override void BeginInteraction(NVRHand hand)
    {
        base.BeginInteraction(hand);

        transform.parent = hand.transform;
        transform.localPosition = positionOffset;
        transform.localRotation = Quaternion.Euler(rotationOffset);
    }

    public void EndGunInteraction()
    {
        ForceDetach();
        gameObject.SetActive(false);
    }


    private void LateUpdate()
    {
        var triggerPressed = AttachedHand != null && (AttachedHand.Inputs[NVRButtons.Trigger].PressDown ||
                                                      Input.GetMouseButtonDown(0));


        if (triggerPressed == true && Time.time - timeSinceLastShot > cooldownTime)
        {
            audioSource.pitch = Random.Range(0.5f, 1.5f);
            audioSource.Play();
            
            laser.enabled = true;
            timeSinceLastShot = Time.time;
            laser.material.SetColor("_Color", laserColor);
            NVRHelpers.LineRendererSetColor(laser, laserColor, laserColor);
            NVRHelpers.LineRendererSetWidth(laser, laserWidth, laserWidth);

            RaycastHit hitInfo;
            bool hit = Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out hitInfo, 1000);
            Vector3 endPoint;

            if (hit == true)
            {
                endPoint = hitInfo.point;
                laserHit(hitInfo, firePoint.transform.forward);
            }
            else
            {
                endPoint = firePoint.transform.position + (firePoint.transform.forward * 1000f);
            }


            laser.SetPositions(new Vector3[] {firePoint.transform.position, endPoint});
        }

        if (laser.enabled && Time.time - timeSinceLastShot > shotDisplayTime)
        {
            laser.enabled = false;
        }
    }

    private void laserHit(RaycastHit hit, Vector3 direction)
    {
        Transform trans = hit.transform;
        var sheep = trans.GetComponentInParent<SheepController>();
        if (sheep != null)
        {
            sheep.OnHit();
        }else if (trans.GetComponent<NVRHead>() != null)
        {
            if(OnShotInHead != null)
                OnShotInHead.Invoke();
        }
        else if (trans.GetComponent<AICompanionController>() != null)
        {
            if (OnShotAI != null)
            {
                OnShotAI.Invoke();
            }
        }else if (hit.rigidbody != null)
        {
            hit.rigidbody.AddForceAtPosition(direction * 100,hit.point);
        }
        else
        {
            var button = trans.GetComponent<CustomButton>();
            if (button != null)
            {
                button.InitiateButtonPress();
            }
        }

        //emitParams.position = hit.point;
        //particleSystem.Emit(emitParams, 20);
        particleSystem.Stop();
        particleSystem.transform.position = hit.point;
        particleSystem.Play();
//        particleSystem.Emit();

    }

    public override void UseButtonDown()
    {
    }

    public void Shred()
    {
        sheepShooterManager.StopAllCoroutines();
        sheepShooterManager.DestroyRemainingSheep();
        sheepShooterManager.laserGunTrigger.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    //do not implement logic here! needs to be empty to avoid gun being removed from hand!
    public override void EndInteraction(NVRHand hand)
    {
    }
}