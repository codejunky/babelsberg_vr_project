﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using UnityEngine;
using UnityEngine.Audio;

public class VolumeSlider : MonoBehaviour
{
    // Start is called before the first frame update
    public NVRSlider slider;
    public AudioMixer radioMixer;


    // Update is called once per frame
    void FixedUpdate()
    {
        var volume = Mathf.Lerp(-40, 10, slider.CurrentValue);
        radioMixer.SetFloat("master_volume", volume);
    }
}
