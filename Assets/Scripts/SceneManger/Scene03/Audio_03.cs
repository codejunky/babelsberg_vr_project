﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_03 : MonoBehaviour
{
    public AudioClip Eating01GoodMorning;
    public AudioClip Eating02PleaseDoNotMind;
    //public AudioClip Eating03aFoodEaten1;
    public AudioClip Eating03aFoodEaten2;
    public AudioClip Eating03aFoodEaten3;
    public AudioClip Eating03aFoodEaten4;
    public AudioClip Eating03bFoodShredded1;
    public AudioClip Eating03bFoodShredded2;
    public AudioClip Eating03bFoodShredded3;
    public AudioClip Eating03cNothingEaten1;
    public AudioClip Eating03cNothingEaten2;
    public AudioClip Eating03cNothingEaten3;
    public AudioClip Eating03dInactive1;
    public AudioClip Eating03dInactive2;
    public AudioClip Eating03dInactive3;

    public AudioClip Eating04EndLevel;

    [Header("Intermission")] public AudioClip Intermission2;
}
