﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController_03 : MonoBehaviour
{
    public bool skipIntro;
    public SceneStates_03 sceneStates;
    public Audio_03 sceneAudio;
    public AudioSource aIAudioSource;
    public AICompanionController aIController;
    public GameObject vendingMachine;
    public Mouth mouth;
    public CameraVFX cameraVFX;
    public List<CustomButton> vendingMachineButtons;
    public AudioSource richardVoice;


    private AudioGlobal audioGlobal;

    private int _playerActiveCount = 0;
    private bool playerActive
    {
        get { return _playerActiveCount > 0; }
        set
        {
            if (value == false)
            {
                _playerActiveCount--;
                playerLastActiveTime = Time.time;
                if (_playerActiveCount < 0) _playerActiveCount = 0;
            }
            else
            {
                _playerActiveCount ++;
            }
        }
    }
    private float playerLastActiveTime = 0;


    private void Awake()
    {
        if (!skipIntro)
        {
            foreach (var button in vendingMachineButtons)
            {
                button.activated = false;
            }
        }
        mouth.OnEating += FoodEaten;
        aIController.OnAiCollisionEnter += OnAiCollisionEnter;
    }

    void Start()
    {
        audioGlobal = GameManager.instance.audioGlobal;
        
        playerActive = true;
        StartCoroutine(LevelSequence());
    }

    private void FixedUpdate()
    {
        if (!playerActive)
        {
            var timeSincePlayerLastActive = Time.time - playerLastActiveTime;
            if (timeSincePlayerLastActive > 10)
            {
                if (sceneStates.pillsEatenCounter == 0)
                {
                    StartCoroutine(NothingEatenReactions());
                }
                else
                {
                    StartCoroutine(InactiveReactions());
                }
            }
        }
    }

    private IEnumerator LevelSequence()
    {
        if (!skipIntro)
        {
            yield return new WaitForSeconds(1);
            yield return richardVoice.PlayClipAndWait(sceneAudio.Intermission2);
        }
        
        yield return new WaitForSeconds(2);
        StartCoroutine(cameraVFX.WakeUp());
        
        if (!skipIntro)
        {
            yield return Play01Intro();
        }

        playerActive = false;

    }
    
    int inactiveReactionCounter = 0;
    IEnumerator InactiveReactions()
    {
        playerActive = true;
        switch (inactiveReactionCounter)
        {
            case 0:
                yield return Speak(sceneAudio.Eating03dInactive1);
                break;
            case 1:
                yield return Speak(sceneAudio.Eating03dInactive2);
                break;
            default:
                yield return Speak(sceneAudio.Eating03dInactive3);
                inactiveReactionCounter = -1;
                break;
        }
        
        inactiveReactionCounter++;
        playerActive = false;
    }

    int nothingEatenReactionCounter = 0;
    IEnumerator NothingEatenReactions()
    {
        //todo: maybe have different reactions if user hasn't pushed button yet
        playerActive = true;
        switch (nothingEatenReactionCounter)
        {
            case 0:
                yield return Speak(sceneAudio.Eating03cNothingEaten1);
                break;
            case 1:
                yield return Speak(sceneAudio.Eating03cNothingEaten2);
                break;
            case 2:
                yield return EndLevel(sceneAudio.Eating03cNothingEaten3);
                break;
        }

        nothingEatenReactionCounter++;
        playerActive = false;
    }

    private int foodThrownReactionCounter = 0;
    private bool foodThrownReactionActive = false;
    IEnumerator FoodThrownReaction()
    {
        if (foodThrownReactionActive) yield break;

        foodThrownReactionActive = true;
        playerActive = true;

        switch (foodThrownReactionCounter)
        {
            case 0:
                yield return Speak(audioGlobal.Eating00aFoodThrown1);
                break;
            case 1:
                yield return Speak(audioGlobal.Eating00aFoodThrown2);
                break;
            default:
                yield return Speak(audioGlobal.Eating00aFoodThrown3);
                foodThrownReactionCounter = -1;
                break;
        }

        foodThrownReactionCounter++;
        playerActive = false;
        foodThrownReactionActive = false;
    }

    private void OnAiCollisionEnter(Collision other)
    {
        if (other.transform.GetComponent<Eatable>() != null)
        {
            StartCoroutine(FoodThrownReaction());
        }
    }

    IEnumerator Speak(AudioClip audioClip)
    {
        if (!sceneStates.endLevelInitiated)
        {
            yield return aIAudioSource.PlayClipAndWait(audioClip);
        }
    }
    
    public void FoodEaten(Eatable food)
    {
        playerActive = true;
        
        if (sceneStates.endLevelInitiated) return;
        
        sceneStates.pillsEatenCounter++;
        switch (sceneStates.pillsEatenCounter)
        {
            case 5:
                StartCoroutine(Speak(sceneAudio.Eating03aFoodEaten2));
                break;
            case 10:
                StartCoroutine(Speak(sceneAudio.Eating03aFoodEaten3));
                break;
            case 15:
                StartCoroutine(TooMuchFoodEnding());
                break;
            default:
                break;
        }

        playerActive = false;
    }

    public void FoodShred()
    {
        playerActive = true;
        
        if (sceneStates.endLevelInitiated) return;
        
        sceneStates.pillsShreddedCounter++;
        switch (sceneStates.pillsShreddedCounter)
        {
            case 1:
                StartCoroutine(Speak(sceneAudio.Eating03bFoodShredded1));
                break;
            case 5:
                StartCoroutine(Speak(sceneAudio.Eating03bFoodShredded2));
                break;
            case 10:
                StartCoroutine(TooMuchShreddedEnding());
                break;
            default:
                break;
        }

        playerActive = false;
    }

    private IEnumerator Play01Intro()
    {
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.Eating01GoodMorning);
        
        foreach (var button in vendingMachineButtons)
        {
            button.activated = true;
        }
    }

    public void Play02ButtonPressed()
    {
        if (!sceneStates.state02VendingMachineButtonPressed && !sceneStates.endLevelInitiated)
        {
            sceneStates.state02VendingMachineButtonPressed = true;
            StartCoroutine(ButtonPressed());
        }
    }

    private IEnumerator ButtonPressed()
    {
        playerActive = true;
        yield return Speak(sceneAudio.Eating02PleaseDoNotMind);
        playerActive = false;
    }

    private IEnumerator TooMuchShreddedEnding()
    {
        yield return EndLevel(sceneAudio.Eating03bFoodShredded3);
    }
    private IEnumerator TooMuchFoodEnding()
    {
        playerActive = true;
        sceneStates.endLevelInitiated = true;
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.Eating03aFoodEaten4);
        sceneStates.endLevelInitiated = false;
        yield return EndLevel(sceneAudio.Eating04EndLevel);
    }

    private IEnumerator EndLevel(AudioClip endLevelAudio)
    {
        playerActive = true;
        if (!sceneStates.endLevelInitiated)
        {
            sceneStates.endLevelInitiated = true;
            
            yield return aIAudioSource.PlayClipAndWait(endLevelAudio);
            yield return cameraVFX.PassOut();
            SceneManager.LoadScene(5);
        }
    }


}
