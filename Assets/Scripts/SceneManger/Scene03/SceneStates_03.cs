﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStates_03 : MonoBehaviour
{
    internal bool state02VendingMachineButtonPressed;
    internal int pillsEatenCounter;
    internal int pillsShreddedCounter;
    internal bool endLevelInitiated;
}
