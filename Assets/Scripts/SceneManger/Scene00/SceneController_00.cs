﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController_00 : MonoBehaviour
{

    private float holdTriggerTime = 3;

    private float pressDuration = 0;

    private bool triggerWasPressed = false;

    // Update is called once per frame
    void Update()
    {
        bool triggerPressed = NVRPlayer.Instance.LeftHand.Inputs[NVRButtons.Trigger].IsPressed ||
                              NVRPlayer.Instance.RightHand.Inputs[NVRButtons.Trigger].IsPressed ||
                              Input.GetMouseButton(0);


        if (triggerPressed)
        {
            pressDuration += Time.deltaTime;
            if (pressDuration > holdTriggerTime)
            {
                SceneManager.LoadScene(1);
            }

        }
        else
        {
            pressDuration = 0;
        }
    }
}
