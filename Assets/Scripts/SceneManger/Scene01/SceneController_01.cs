﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using NewtonVR;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;

public class SceneController_01 : MonoBehaviour
{

    public bool skipIntro;
    public SceneStates_01 sceneStates;
    public Audio_01 sceneAudio;
    public AudioSource aIAudioSource;
    public GameObject musicPlayer;
    public CustomButton powerButton;
    public NVRSlider slider;
    public CameraVFX cameraVFX;

    private void Awake()
    {
        musicPlayer.SetActive(false);
        slider.gameObject.SetActive(false);
        musicPlayer.SetCollisions(false);
    }

    void Start()
    {
        StartCoroutine(Play01Intro());
    }

    IEnumerator Play01Intro()
    {
        if (!skipIntro)
        yield return new WaitForSeconds(2);
        
        StartCoroutine(cameraVFX.WakeUp());

        if (!skipIntro)
        {
            yield return aIAudioSource.PlayClipAndWait(sceneAudio.intro01GoodMorning);
        }
        StartCoroutine(AnimateMusicPlayer());
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.intro02PleaseStepUp);
    }

    public void Play02MusicPlayerStarted()
    {
        if (!sceneStates.state02MusicplayerStarted)
        {
            print("MusicPlayer Started!!!");
            sceneStates.state02MusicplayerStarted = true;
            powerButton.activated = false;
            StartCoroutine(MusicPlayerSequence());
        }
    }

    IEnumerator MusicPlayerSequence()
    {
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.intro03VeryGood);
        yield return new WaitForSeconds(2);
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.intro04ToMaximize);
        yield return new WaitForSeconds(5);
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.intro05ISincerlyHope);
        yield return new WaitForSeconds(10);
        musicPlayer.GetComponent<Jukebox>().TurnOff();
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.intro06ThankYouForParticipating);
        yield return cameraVFX.PassOut();
        SceneManager.LoadScene(2);
    }


    private IEnumerator AnimateMusicPlayer()
    {
        var musicPlayerYOffset = -0.6f;
        var speed = .8f;
        var endPos = musicPlayer.transform.position;
        musicPlayer.transform.Translate(0, musicPlayerYOffset, 0);
        var startPos = musicPlayer.transform.position;
        var startTime = Time.time;

        musicPlayer.SetActive(true);
        
        //glitchy slider workaround
        var copiedSlider = Instantiate(slider.gameObject, slider.transform.parent).GetComponent<NVRSlider>();
        copiedSlider.SetValue(copiedSlider.startValue);
        copiedSlider.enabled = false;
        //

        while (true)
        {
            float t = (Time.time - startTime) * speed;
            musicPlayer.transform.position = Vector3.Lerp(startPos, endPos, t);
            if (t >= 1)
            {
                //glitchy slider workaround
                musicPlayer.SetActive(false);
                musicPlayer.SetActive(true);
                musicPlayer.SetCollisions(true);
                Destroy(copiedSlider.gameObject);
                slider.gameObject.SetActive(true);
                //
                yield break;
            }

            yield return null; // Wait for the next frame before looping over
        }
    }
}