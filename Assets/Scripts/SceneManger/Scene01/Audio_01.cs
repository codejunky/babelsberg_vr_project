﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_01 : MonoBehaviour
{
    public AudioClip intro01GoodMorning;
    public AudioClip intro02PleaseStepUp;
    public AudioClip intro03VeryGood;
    public AudioClip intro04ToMaximize;
    public AudioClip intro05ISincerlyHope;
    public AudioClip intro06ThankYouForParticipating;
}
