﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Audio_02 : MonoBehaviour
{
    //TODO: add missing sound clips for travelling02aForest2, 3 etc:
    public AudioClip travelling01GoodMorning;
    [FormerlySerializedAs("travelling02aForest")] public AudioClip travelling02aForest1;
    public AudioClip travelling02aForest2;
    public AudioClip travelling02aForest3;
    [FormerlySerializedAs("travelling02bDesert")] public AudioClip travelling02bDesert1;
    public AudioClip travelling02bDesert2;
    public AudioClip travelling02bDesert3;
    [FormerlySerializedAs("travelling02cSpace")] public AudioClip travelling02cSpace1;
    public AudioClip travelling02cSpace2;
    public AudioClip travelling02cSpace3;
    public AudioClip travelling03TryAnotherOne1;
    public AudioClip travelling03TryAnotherOne2;
    public AudioClip travelling04EndLevel;
    
    [Header("Intermission")]
    public AudioClip intermission1;

}
