﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using DefaultNamespace;
using NewtonVR;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public class SceneController_02 : MonoBehaviour
{
    public SceneStates_02 sceneStates;
    public Audio_02 sceneAudio;
    public AudioSource aIAudioSource;
    public GameObject travellingButtons;
    public CameraVFX camVFX;
    private CustomButton[] buttons;
    public AudioSource richardVoice;

    private void Awake()
    {
        buttons = travellingButtons.GetComponentsInChildren<CustomButton>();
        DeactivateButtons();
    }

    void Start()
    {
        
        StartCoroutine(Play01Intro());
    }

    IEnumerator Play01Intro()
    {
        yield return new WaitForSeconds(1);
        yield return richardVoice.PlayClipAndWait(sceneAudio.intermission1);
        yield return new WaitForSeconds(2);

        
        StartCoroutine(camVFX.WakeUp());
        
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling01GoodMorning);
        ActivateButtons();
    }

    public void Play02aForest()
    {
        DeactivateButtons();
        StartCoroutine(Forest());
    }

    private IEnumerator Forest()
    {
        switch (sceneStates.forestVisitedCounter)
        {
            case 0:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02aForest1);
                break;
            case 1:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02aForest2);
                break;
            case 2:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02aForest3);
                break;
            default:
                Debug.LogError("Unexpected state of forestVisitedCounter: " + sceneStates.forestVisitedCounter);
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02aForest1);
                break;
                
        }
        sceneStates.buttonsPressedCounter++;
        sceneStates.forestVisitedCounter++;
        yield return NextState();
    }
    
    public void Play02bDesert()
    {
        DeactivateButtons();
        StartCoroutine(Desert());
    }
    
    private IEnumerator Desert()
    {
        switch (sceneStates.desertVisitedCounter)
        {
            case 0:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02bDesert1);
                break;
            case 1:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02bDesert2);
                break;
            case 2:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02bDesert3);
                break;
            default:
                Debug.LogError("Unexpected state of desertVisitedCounter: " + sceneStates.desertVisitedCounter);
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02bDesert1);
                break;
        }
        sceneStates.buttonsPressedCounter++;
        sceneStates.desertVisitedCounter++;
        yield return NextState();
    }
    
    public void Play02cSpace()
    {
        DeactivateButtons();
        StartCoroutine(Space());
    }
    
    private IEnumerator Space()
    {
        switch (sceneStates.spaceVisitedCounter)
        {
            case 0:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02cSpace1);
                break;
            case 1:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02cSpace2);
                break;
            case 2:
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02cSpace3);
                break;
            default:
                Debug.LogError("Unexpected state of spaceVisitedCounter: " + sceneStates.spaceVisitedCounter);
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling02cSpace1);
                break;
        }
        sceneStates.buttonsPressedCounter++;
        sceneStates.spaceVisitedCounter++;
        yield return NextState();
    }

    public IEnumerator NextState()
    {        
        yield return new WaitForSeconds(2);
        switch (sceneStates.buttonsPressedCounter)
        {
            case 1:
                ActivateButtons();
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling03TryAnotherOne1);
                break;
            case 2:
                ActivateButtons();
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling03TryAnotherOne2);
                break;
            case 3:
                yield return EndLevel();
                break;
            default:
                Debug.LogError("buttonsPressedCounter is in unexpected state: " + sceneStates.buttonsPressedCounter);
                yield return EndLevel();
                break;
        }

    }

    private IEnumerator EndLevel()
    {
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.travelling04EndLevel);
        yield return camVFX.PassOut();
        
        SceneManager.LoadScene(3);
    }
    
    


    private void ActivateButtons()
    {
        foreach (var button in buttons)
        {
            button.activated = true;
        }
    }

    private void DeactivateButtons()
    {
        foreach (var button in buttons)
        {
            button.activated = false;
        }
    }
    
}
