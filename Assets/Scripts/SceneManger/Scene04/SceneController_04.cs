﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController_04 : MonoBehaviour
{
    public SceneStates_04 sceneStates;
    public Audio_04 sceneAudio;
    public AudioSource aIAudioSource;
    public CameraVFX camVFX;
    public AudioSource richardVoice;

    void Start()
    {
        StartCoroutine(Play01Intro());
    }

    IEnumerator Play01Intro()
    {
        yield return new WaitForSeconds(2);
        yield return camVFX.WakeUp();
//        yield return aIAudioSource.PlayClipAndWait(sceneAudio.shaming01GoodMorning);
        yield return EndLevel();
    }

    IEnumerator EndLevel()
    {
        yield return camVFX.PassOut();
        
        SceneManager.LoadScene(5);
    }

}
