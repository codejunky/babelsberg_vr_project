﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class SceneController_99 : MonoBehaviour
{

    public AudioSource richardsVoice;
    public AudioSource endExplosion;


    public AudioClip endAudio;
    public AudioClip explosionAudio;

    
    public GameObject endText;

    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EndSequence());
    }

    IEnumerator EndSequence()
    {
        yield return new WaitForSeconds(1);
        yield return richardsVoice.PlayClipAndWait(endAudio);
        print("play");
        endText.SetActive(true);
        yield return endExplosion.PlayClipAndWait(explosionAudio);
    }
}
