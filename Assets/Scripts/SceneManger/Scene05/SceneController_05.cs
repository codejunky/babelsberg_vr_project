﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController_05 : MonoBehaviour
{
    public bool skipIntro = false;
    
    public SceneStates_05 sceneStates;
    public Audio_05 sceneAudio;
    public AudioSource aIAudioSource;
    public AICompanionController aiCompanion;
    public Transform aiTargetTrans;
    public GameObject laserGunTrigger;
    public CameraVFX cameraVFX;
    public SheepShooterManager sheepShooterManager;

    public LaserGun laserGun;
    public AudioSource richardVoice;

    private Vector3 aiInitialPos = Vector3.zero;

    private AudioGlobal audioGlobal;

    private void Awake()
    {
        laserGunTrigger.SetActive(false);
        laserGun.OnShotInHead += OnShotInHead;
        laserGun.OnShotAI += OnShotAi;
    }

    void Start()
    {
        audioGlobal = GameManager.instance.audioGlobal;

        StartCoroutine(LevelSequence());
    }

    private IEnumerator LevelSequence()
    {
        if (!GameManager.instance.statesGlobal.SuicideReaction && !skipIntro)
        {
            yield return new WaitForSeconds(1);
            yield return richardVoice.PlayClipAndWait(sceneAudio.Intermission3);
        }

        yield return new WaitForSeconds(2);
        StartCoroutine(cameraVFX.WakeUp());
        if (!skipIntro)
        {
            yield return Play01Intro();
        }
        StartCoroutine(AnimateGunTrigger());
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepShooter01bPickUpTheGun);

    }

    IEnumerator Play01Intro()
    {
        if (!GameManager.instance.statesGlobal.SuicideReaction)
            yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepShooter01aGoodMorning);
        else
            yield return SuicideReactions();

    }
    
    private IEnumerator AnimateGunTrigger()
    {
        print("start");
        var gunYOffset = -0.05f;
        var speed = 2f;
        var endPos = laserGunTrigger.transform.position;
        laserGunTrigger.transform.Translate(0, gunYOffset, 0, Space.World);
        var startPos = laserGunTrigger.transform.position;
        var startTime = Time.time;

        laserGunTrigger.SetActive(true);

        while (true)
        {
            float t = (Time.time - startTime) * speed;
            laserGunTrigger.transform.position = Vector3.Lerp(startPos, endPos, t);
            yield return null; // Wait for the next frame before looping over
        }
    }
    
    public void OnGameStarted()
    {
        StartCoroutine(Play02StartGame());
    }
    
    IEnumerator Play02StartGame()
    {
        aiInitialPos = aiCompanion.parentTransform.position;
        StartCoroutine(aiCompanion.MoveTo(aiTargetTrans.position, 2));
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepShooter02StartGame);
    }

    private void OnShotInHead()
    {
        GameManager.instance.statesGlobal.SuicideReaction = true;
        SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
    }

    private void OnShotAi()
    {
        StartCoroutine(AiShotReactions());
    }

    private int aiShotCounter = 0;
    private bool reactingToBeingShot = false;
    private IEnumerator AiShotReactions()
    {
        if(reactingToBeingShot) yield break;

        reactingToBeingShot = true;
        switch (aiShotCounter)
        {
            case 0: 
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00bShotAi1);
                print("ai shot 1");
                break;
            case 1:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00bShotAi2);
                print("ai shot 2");
                break;
            default:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00bShotAi3);
                print("ai shot 3");
                aiShotCounter = -1;
                break;
        }
        aiShotCounter++;
        reactingToBeingShot = false;
    }

    IEnumerator SuicideReactions()
    {
        switch (GameManager.instance.statesGlobal.SuicideReactionCounter)
        {
            case 0:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00aSuicide1);
                print("suicide1");
                break;
            case 1:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00aSuicide2);
                print("suicide2");
                break;
            default:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00aSuicide3);
                GameManager.instance.statesGlobal.SuicideReactionCounter = -1;
                print("suicide3");
                break;
        }

        GameManager.instance.statesGlobal.SuicideReactionCounter++;
        GameManager.instance.statesGlobal.SuicideReaction = false;

    }

    IEnumerator GamescoreReactions()
    {
        var score = sheepShooterManager.finalScore;

        if (score > 50)
        {
            yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepshooter03aHighScore);
        }
        else if (score > 25)
        {
            yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepshooter03bMediumScore);
        }
        else if (score > 0)
        {
            yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepshooter03cLowScore);
        }
        else
        {
            yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepshooter03dZeroPoints);
        }
    }


    public void OnGameFinished()
    {
        StartCoroutine(EndLevel());
    }
    
    private IEnumerator EndLevel()
    {
        StartCoroutine(aiCompanion.MoveTo(aiInitialPos, 0.7f));
        yield return GamescoreReactions();
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.sheepshooter04EndLevel);

        yield return cameraVFX.PassOut();
        SceneManager.LoadScene(6);
    }
    
    
}
