﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_05 : MonoBehaviour
{   
    public AudioClip sheepShooter01aGoodMorning;
    public AudioClip sheepShooter01bPickUpTheGun;
    
    public AudioClip sheepShooter02StartGame;

    public AudioClip sheepshooter03aHighScore;
    public AudioClip sheepshooter03bMediumScore;
    public AudioClip sheepshooter03cLowScore;
    public AudioClip sheepshooter03dZeroPoints;

    public AudioClip sheepshooter04EndLevel;
    
    [Header("Intermission")] public AudioClip Intermission3;
}
