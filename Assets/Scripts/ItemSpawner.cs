﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class ItemSpawner : MonoBehaviour, TriggerableObjectInterface
{
    public GameObject objectToSpawn;
    public Transform SpawnLocation;

    public void TriggerStart()
    {
        SpawnObject(objectToSpawn);
    }

    void SpawnObject(GameObject objectToSpawn)
    {
        var spawnedObject = Instantiate(objectToSpawn);
        spawnedObject.transform.position = SpawnLocation.position;
    }

    public void TriggerEnd()
    {
    }
}