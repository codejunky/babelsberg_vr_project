﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioGlobal : MonoBehaviour
{
    [Header("Eating")]
    public AudioClip eating_chew;
    public AudioClip shredder_shred;
    public AudioClip shredder_breaks;
    public AudioClip Eating00aFoodThrown1;
    public AudioClip Eating00aFoodThrown2;
    public AudioClip Eating00aFoodThrown3;
    
    [Header("SheepShooter")]
    public AudioClip sheep_laserGunShot;
    public AudioClip sheep_bark;
    public AudioClip sheep_explosion;
    
    public AudioClip sheepShooter00aSuicide1;
    public AudioClip sheepShooter00aSuicide2;
    public AudioClip sheepShooter00aSuicide3;
    public AudioClip sheepShooter00bShotAi1;
    public AudioClip sheepShooter00bShotAi2;
    public AudioClip sheepShooter00bShotAi3;
    
    [Header("Button")]
    public AudioClip button_pressedActive;
    public AudioClip button_pressedInactive;

    [Header("Player")] 
    public AudioClip player_injection;

    public AudioClip player00astuffThrown1;
    public AudioClip player00astuffThrown2;
    public AudioClip player00astuffThrown3;

}
