namespace DefaultNamespace
{
    public interface TriggerableObjectInterface
    {
        void TriggerStart();
        void TriggerEnd();
    }
}