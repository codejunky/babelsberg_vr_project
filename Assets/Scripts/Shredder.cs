﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using UnityEngine;
using UnityEngine.Events;

public class Shredder : MonoBehaviour
{
    public ParticleSystem shredParticles;
    public ParticleSystem sparkParticles;
    public ParticleSystem smokeParticles;

    private AudioSource audioSource;
    private AudioClip shredderAudio;
    private AudioClip shredderBreaksAudio;
    public UnityEvent OnShred;
    public UnityEvent OnShredderBroken;

    private bool shredderBroken = false;
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        shredderAudio = GameManager.instance.audioGlobal.shredder_shred;
        shredderBreaksAudio = GameManager.instance.audioGlobal.shredder_breaks;
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (shredderBroken) return;
        
        var interactableItem = other.GetComponent<NVRInteractableItem>();
        if (interactableItem != null && interactableItem.canBeShredded)
        {
            
            if (interactableItem.breaksShredder)
            {
                ShredderBreaks();
            }
            else
            {
                interactableItem.Shred();
                Shred();
            }

        }
        else
        {
            var lasergun = other.GetComponent<LaserGun>();
            if (lasergun != null)
            {
                lasergun.Shred();
                ShredderBreaks();
            }
        }
    }

    private void ShredderBreaks()
    {
        shredderBroken = true;
        
        audioSource.clip = shredderBreaksAudio;
        audioSource.Play();
        sparkParticles.Play();
        smokeParticles.Play();
        OnShredderBroken.Invoke();
    }

    private void Shred()
    {
        audioSource.clip = shredderAudio;
        audioSource.Play();
        shredParticles.Play();
        OnShred.Invoke();
    }
}
