﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using NewtonVR.Example;
using UnityEngine;

public class LaserGunTrigger : NVRInteractable
{
	public NVRInteractable laserGun;
	public SheepShooterManager sheepShooterManager;


	public override void BeginInteraction(NVRHand hand)
	{
		laserGun.gameObject.SetActive(true);
		hand.BeginInteraction(laserGun);
		ForceDetach(hand);
		sheepShooterManager.StartGame();
		gameObject.SetActive(false);
	}
	public void ResetTrigger()
	{
		gameObject.SetActive(true);
	}

}
