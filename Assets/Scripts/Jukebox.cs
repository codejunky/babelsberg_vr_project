﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Jukebox : MonoBehaviour
{
    public bool alwaysOn;
    public GameObject speaker;

    private void Awake()
    {
        
    }
    
    public void TogglePower()
    {
        if (speaker.activeSelf)
        {
            if (!alwaysOn)
            {
                TurnOff();
            }
        }
        else
        {
            speaker.SetActive(true);
        }
    }

    public void TurnOff()
    {
        speaker.SetActive(false);
    }
}
