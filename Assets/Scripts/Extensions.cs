using System.Collections;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace DefaultNamespace
{
    public static class Extensions
    {
        public static void SetCollisions(this GameObject gameObject, bool collisionEnabled)
        {
            gameObject.GetComponentsInChildren<BoxCollider>().ForEach(coll => coll.enabled = collisionEnabled);
            gameObject.GetComponentsInChildren<SphereCollider>().ForEach(coll => coll.enabled = collisionEnabled);
            gameObject.GetComponentsInChildren<MeshCollider>().ForEach(coll => coll.enabled = collisionEnabled);
        }
        
        public static IEnumerator PlayClipAndWait(this AudioSource audioSource, AudioClip clip)
        {
            audioSource.clip = clip;
            audioSource.Play();
            yield return new WaitForSeconds(clip.length);
        }

    }
    
    
}