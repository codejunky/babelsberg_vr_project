﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using UnityEngine;
using UnityEngine.Audio;

public class ChangeStationKnob : MonoBehaviour
{
    public Transform knobTransform;
    public AudioMixer radioMixer;


    private string radioStaticVol = "radiostatic_volume";
    private string danceMusicVol = "dance_music_volume";
    private string dubstepMusicVol = "dubstep_music_volume";
    private string dubstepShortVol = "dubstep_short_volume";
    
    private TextMesh text;
    private float musicMute = -80;
    private float staticMute = -40;
    private float musicNormal = 0;
    private float staticNormal = 0;


    private void FixedUpdate()
    {
        var degrees = knobTransform.localEulerAngles.y;

        //dance music to static
        if (degrees >= 0 && degrees < 60)
        {
            var percent = degrees / 60;
            InterpolateStatic(percent, true);
            radioMixer.SetFloat(danceMusicVol, musicNormal);

            //turn off
            radioMixer.SetFloat(dubstepMusicVol, musicMute);
            radioMixer.SetFloat(dubstepShortVol, musicMute);
        }
        
        //static to dubstep music
        else if (degrees >= 60 && degrees < 120)
        {
            var percent = (degrees - 60)/ 60;
            InterpolateStatic(percent, false);
            radioMixer.SetFloat(dubstepMusicVol, musicNormal);
            
            radioMixer.SetFloat(danceMusicVol, musicMute);
            radioMixer.SetFloat(dubstepShortVol, musicMute);
        }
        
        //dubstep music to static
        else if (degrees >= 120 && degrees < 180)
        {
            var percent = (degrees - 120)/ 60;
            InterpolateStatic(percent, true);
            radioMixer.SetFloat(dubstepMusicVol, musicNormal);

            radioMixer.SetFloat(danceMusicVol, musicMute);
            radioMixer.SetFloat(dubstepShortVol, musicMute);
            
        }
        
        //static to dubstep short
        else if (degrees >= 180 && degrees < 240)
        {
            var percent = (degrees - 180)/ 60;
            InterpolateStatic(percent, false);
            radioMixer.SetFloat(dubstepShortVol, musicNormal);

            radioMixer.SetFloat(dubstepMusicVol, musicMute);
            radioMixer.SetFloat(danceMusicVol, musicMute);
        }
        
        //dubstep short to static
        else if (degrees >= 240 && degrees < 300)
        {
            var percent = (degrees - 240)/ 60;
            InterpolateStatic(percent, true);
            radioMixer.SetFloat(dubstepShortVol, musicNormal);

            radioMixer.SetFloat(dubstepMusicVol, musicMute);
            radioMixer.SetFloat(danceMusicVol, musicMute);
        }
        
        //static to dance music
        else if (degrees >= 300 && degrees < 360)
        {
            var percent = (degrees - 300)/ 60;
            InterpolateStatic(percent, false);
            radioMixer.SetFloat(danceMusicVol, musicNormal);

            radioMixer.SetFloat(dubstepMusicVol, musicMute);
            radioMixer.SetFloat(dubstepShortVol, musicMute);

        }
    }

    private void InterpolateStatic(float percent, bool musicToStatic)
    {
        if (!musicToStatic) percent = 1 - percent;
        
        var volumeBval = Mathf.Lerp(staticMute, staticNormal, percent);
        radioMixer.SetFloat(radioStaticVol, volumeBval);
    }


    
}
