﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using TMPro;
using UnityEngine;

public class ChangeEnvironment : MonoBehaviour, TriggerableObjectInterface
{


	public GameObject environmentToChangeTo;
	private List<GameObject> environments;


	// Use this for initialization
	void Start ()
	{
		environments = GetComponentInParent<TravellingEnvironments>().environments;
	}
	

	private void changeEnvironment()
	{
		foreach (var environment in environments)
		{
			if (environment == environmentToChangeTo)
			{
				environment.SetActive(true);
			}
			else
			{
				environment.SetActive(false);
			}
		}
	}

	public void TriggerStart()
	{
		changeEnvironment();
	}

	public void TriggerEnd()
	{
	}
}
