﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using NewtonVR;
using UnityEngine;
using UnityEngine.Serialization;

public class SteamVREmulatorController : MonoBehaviour
{
    private NVRHand hand;
    private NVRPlayer player;
    public float handMoveSpeed = 0.5f;
    public float handRotateSpeed = 10;
    public float defaultHandDist = 0.5f;

    public float movementSpeed = 1.0f;
    public float shiftSpeed = 4.0f;

    private Vector3 startEulerAngles;
    private Vector3 startMousePosition;
    private bool emulatorEnabled = false;


    void Start()
    {
        player = GetComponent<NVRPlayer>();
        hand = player.LeftHand;
//        emulatorEnabled = (player.CurrentIntegrationType == NVRSDKIntegrations.FallbackNonVR);


        //TODO: maybe find a different solution for this?
        hand.transform.parent = player.Head.transform;
        hand.transform.Translate(0, 0, defaultHandDist);
    }

    void Update()
    {
        InteractionControls();
        if (Input.GetKey(KeyCode.Space))
        {
            AlternatelHandControls();
        }
        else
        {
            HandControls();
            CameraControls();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ResetHand();
        }
    }

    void InteractionControls()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            hand.PickupClosest();
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            hand.EndInteraction(null);
        }
    }


    void HandControls()
    {
        float handDist = 0;
        if (Input.GetKey(KeyCode.E))
        {
            handDist += 1.0f;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            handDist -= 1.0f;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (hand.CurrentlyInteracting != null)
            {
                hand.CurrentlyInteracting.UseButtonDown();
            }
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (hand.CurrentlyInteracting != null)
            {
                hand.CurrentlyInteracting.UseButtonUp();
            }
        }


        handDist *= handMoveSpeed * Time.deltaTime;
        hand.transform.Translate(0, 0, handDist, Space.Self);
    }

    void AlternatelHandControls()
    {
        float up = 0.0f;
        if (Input.GetKey(KeyCode.W))
        {
            up += 1.0f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            up -= 1.0f;
        }

        float right = 0.0f;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            right += 1.0f;
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            right -= 1.0f;
        }

        float rotateX = 0.0f;
        if (Input.GetKey(KeyCode.E))
        {
            rotateX += 1.0f;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            rotateX -= 1.0f;
        }

        float rotateY = 0.0f;
        if (Input.GetKey(KeyCode.R))
        {
            rotateY += 1.0f;
        }

        if (Input.GetKey(KeyCode.F))
        {
            rotateY -= 1.0f;
        }

        right *= handMoveSpeed * Time.deltaTime;
        up *= handMoveSpeed * Time.deltaTime;
        hand.transform.Translate(new Vector3(right, up, 0), Space.Self);

        rotateX *= handRotateSpeed * Time.deltaTime;
        rotateY *= handRotateSpeed * Time.deltaTime;
        //TODO: rotation messes up movement and stuff, maybe use local rotation or something else create new parent and move parent wile rotating child
        hand.transform.Rotate(-rotateY, 0, -rotateX);
    }

    void CameraControls() //stolen from SteamVR's FallbackCameraController
    {
        float forward = 0.0f;
        if (Input.GetKey(KeyCode.W))
        {
            forward += 1.0f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            forward -= 1.0f;
        }

        float right = 0.0f;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            right += 1.0f;
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            right -= 1.0f;
        }

        float up = 0.0f;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            up += 1.0f;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            up -= 1.0f;
        }

        float currentSpeed = movementSpeed;
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            currentSpeed = shiftSpeed;
        }


        Vector3 delta = new Vector3(right, up, forward) * currentSpeed * Time.deltaTime;

        transform.position += transform.TransformDirection(delta);

        Vector3 mousePosition = Input.mousePosition;

        if (Input.GetMouseButtonDown(1) /* right mouse */)
        {
            startMousePosition = mousePosition;
            startEulerAngles = transform.localEulerAngles;
        }

        if (Input.GetMouseButton(1) /* right mouse */)
        {
            Vector3 offset = mousePosition - startMousePosition;
            transform.localEulerAngles = startEulerAngles + new Vector3(-offset.y * 360.0f / Screen.height,
                                             offset.x * 360.0f / Screen.width, 0.0f);
        }
    }

    void ResetHand()
    {
        hand.transform.localPosition = new Vector3(0, 0, defaultHandDist);
        hand.transform.localRotation = Quaternion.Euler(0, 0, 0);
    }
}