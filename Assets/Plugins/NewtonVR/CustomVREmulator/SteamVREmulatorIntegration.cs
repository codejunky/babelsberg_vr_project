using UnityEngine;

namespace NewtonVR
{
    public class SteamVREmulatorIntegration : NVRIntegration
    {
        public override void Initialize(NVRPlayer player)
        {
            Player = player;

            Player.LeftHand.CustomModel = SetupSteamVRRenderModel(Player.LeftHand);

            // Setup left hand
            Player.CurrentIntegrationType = NVRSDKIntegrations.SteamVR;
            Player.LeftHand.PreInitialize(Player);
            Player.LeftHand.Initialize();

            // Setup right hand
//            Player.RightHand.CustomModel = SetupSteamVRRenderModel(Player.RightHand);
//            Player.RightHand.PreInitialize(Player);
//            Player.RightHand.Initialize();
        }

        private GameObject SetupSteamVRRenderModel(NVRHand hand)
        {
            GameObject renderModel = new GameObject("Render Model for " + hand.gameObject.name);
            renderModel.transform.parent = hand.transform;
            renderModel.transform.localPosition = Vector3.zero;
            renderModel.transform.localRotation = Quaternion.identity;
            renderModel.transform.localScale = Vector3.one;
            renderModel.AddComponent<SteamVR_RenderModel>();
            renderModel.GetComponent<SteamVR_RenderModel>().shader = Shader.Find("Standard");

            return renderModel;
        }

        public override Vector3 GetPlayspaceBounds()
        {
            return Vector3.zero;
        }

        public override bool IsHmdPresent()
        {
            return false;
        }
    }
}