using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AISpeechClip : PlayableAsset, ITimelineClipAsset
{
    public AISpeechBehaviour template = new AISpeechBehaviour ();
    private AudioClip m_Clip;

    public ClipCaps clipCaps
    {
        get { return ClipCaps.ClipIn; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AISpeechBehaviour>.Create (graph, template);
        AISpeechBehaviour clone = playable.GetBehaviour ();
        m_Clip = clone.AudioClip;
        return playable;
    }    

    public override double duration
    {
        get
        {
            if (m_Clip == null)
                return base.duration;
 
            // use this instead of length to avoid rounding precision errors,
            return (double)m_Clip.samples / (double)m_Clip.frequency;
        }
    }
    
    
    
    
}
