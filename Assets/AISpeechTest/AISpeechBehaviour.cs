using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AISpeechBehaviour : PlayableBehaviour
{
    public AudioClip AudioClip;

    public override void OnPlayableCreate (Playable playable)
    {
        
    }
}
